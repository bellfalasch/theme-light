<?php
/*
   Template Name: Error
	<title>Kritiskt server-fel - FFUniverse.nu</title>
*/
?><?php get_header(); ?>

<main class="not-found">
	<div class="container">
    <div class="row">
      <div class="col-12">
        <h1>Sidan kraschade, kupo!</h1>
        <p class="secondary-message">Status: <strong>50X</strong></p>
        <img src="<?= get_template_directory_uri() ?>/assets/images/magic-pot.png" alt="Pixelbild på Magic Urn/Pot, från Final Fantasy-spelen" style="float:right; margin: 0 0 6px 6px;" />
        <p>
          Sorry, men nu har vi gjort bort oss lite - denna sida innehåller kod som kraschar!
        </p>
        <p><strong>Men pröva gärna att ...</strong></p>
        <ul>
          <li>Gå tillbaka till <a href="#" onclick="window.history.back();">sidan du kom ifrån</a> och försök med en annan länk.</li>
          <li>Dubbelkolla så att adressen verkligen är rättstavad.</li>
          <li>Rapportera felet (se informationen nedan).</li>
          <li>Pröva att gå <a href="<?= get_option('home'); ?>/">tillbaks till startsidan</a></li>
          <li>Spela lite <a href="https://guide.ffuniverse.nu/">Final Fantasy</a> istället och försök igen lite senare.</li>
        </ul>
        <p class="center">
          För att rapportera felet, så att vi kan laga det, ska du kontakta
          <a href="mailto:webmaster@ffuniverse.nu" style="color:#ea218e">Webmaster</a>.
          I det mailet ska du inkludera hela den detaljerade informationen ovan.
        </p>
      </div>
    </div>
	</div>
</main>

<?php get_footer(); ?>
