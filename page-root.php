<?php
/*
   Template Name: Choose your path
	<title>Final Fantasy, Kingdom Hearts, Square Enix - svenska guider och nyheter</title>
*/
?><?php get_header(); ?>

<main class="ffu-root">
	<div class="container">
       <div class="row">
         <div class="col-12">
           <h1>Final Fantasy Universe <small>v&auml;lj din v&auml;g</small></h1>
           <p>
             <strong>FFUniverse.nu</strong> har i &ouml;ver 20 &aring;r skrivit <strong>spelguider</strong> f&ouml;r att hj&auml;lpa dig igenom <strong>Final Fantasy</strong>-spelen, och m&aring;nga andra spel fr&aring;n <strong>Square Enix</strong>.
             Vi skriver &auml;ven <strong>nyheter</strong> och har en <strong>community</strong> f&ouml;r FF-intresserade.
           </p>
           <p class="center">
             B&ouml;rja med att v&auml;lja din v&auml;g:
           </p>
         </div>
       </div>
	</div>

	<div class="container-full">
		<a href="https://guide.ffuniverse.nu" class="banner guides">
			<div class="bg"></div>
			<div class="inner">
				<h2>Guider</h2>
				<p>K&ouml;rt fast i ett Final Fantasy, Kingdom Hearts, Chrono Trigger, Bravely Default, eller annat Square Enix-spel? Fastnat p&aring; en boss, beh&ouml;ver ett speciellt vapen, vill veta alla hemligheter? V&aring;ra 20+ guider har &ouml;ver 1000 sidor!</p>
			</div>
		</a>

		<a href="https://nyheter.ffuniverse.nu" class="banner news">
			<div class="bg"></div>
			<div class="inner">
				<h2>Nyheter</h2>
				<p>F&aring; med dig det senaste om Final Fantasy, Kingdom Hearts, och allt annat Square Enix-relaterat. Vi har producerat &ouml;ver 1500 artiklar. Hett just nu &auml;r FF7 Remake, Kingdom Hearts 3, och Final Fantasy XVI.</p>
			</div>
		</a>
	</div>

	<div class="container-grid">
		<a href="http://www.ffuniverse.nu/community/" class="banner community">
			<h2>Community</h2>
			<p>Bli medlem och diskutera FF med andra fans. Vi har chatt, forum, <span class="hidden-sm">egen sida, v&auml;nnerfunktion, fr&aring;gesport, </span>med mera. Och s&aring; tr&auml;ffas vi en g&aring;ng i &aring;ret!</p>
		</a>

		<a href="https://om.ffuniverse.nu/" class="banner about">
			<h3>Om oss</h3>
			<p>Vanliga fr&aring;gor, kontakt, och annan information om oss.</p>
		</a>

		<a href="https://om.ffuniverse.nu/blogg/" class="banner dev">
			<h3>Blogg</h3>
			<p>En teknisk blogg om utvecklingen bakom FFU.</p>
		</a>
	</div>
</main>

<?php get_footer(); ?>
