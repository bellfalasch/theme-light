<?php
/*
   Template Name: Community
   	<title>Sidan kunde inte hittas - FFUniverse.nu</title>
*/
?><?php get_header(); ?>

<main class="community">
	<div class="container">
    <div class="row">
      <div class="col-12 center">
        <h1>Communityn</h1>
        <p>
          Communityn var ute och gick i en grotta och träffade på Omega Weapon.<br />
          Tyvärr var den helt oförberedd och dog i denna strid.<br />
          Eftersom vi inte har fler Phoenix Down kvar att ge så låter vi communityn somna in, för evigt.<br />
          Vila i frid!
        </p>
        <p>
          Stort tack till communityn för 19 år i tjänst, och <strong>tack till alla er</strong> som använde den och älskade den.
          Som träffades där, blev vänner, hittade ljus i mörkret, blev kära, skapade liv tillsammans.
        </p>
        <p>
          R.I.P. 2002 - 2021
        </p>
        <p>
          (PS. De flesta användarna finns samlade tryggt på <a href="https://www.facebook.com/finalfantasy.nu">Facebook - följ oss</a>!)
        </p>
      </div>
    </div>
  </div>
</main>

<?php get_footer(); ?>
