<?php get_header(); ?>

<main class="not-found">
	<div class="container">
    <div class="row">
      <div class="col-12">
        <h1>Vi finner inte sidan, kupo!</h1>
        <p class="secondary-message">Status: <strong>404</strong></p>
        <img src="<?= get_template_directory_uri() ?>/assets/images/magic_pot.png" alt="Pixelbild på Magic Urn/Pot, från Final Fantasy-spelen" style="float:right; margin: 0 0 6px 6px;" />
        <p>
          Tyvärr kan inte sidan du sökte efter hittas hos oss längre.
          Kanske den flyttats?!?
          Kanske den aldrig fanns?!
          Kanske en hungrig Moogle åt upp den?
          Kanske Magic Urn har tagit den
        </p>
        <p>
          Vad som än hänt den så är den inte här, kupo!
        </p>

        <p><strong>Men pröva gärna att ...</strong></p>
        <ul>
          <li>Gå tillbaka till <a href="#" onclick="window.history.back();">sidan du kom ifrån</a> och försök med en annan länk.</li>
          <li>Dubbelkolla så att adressen verkligen är rättstavad.</li>
          <li>Rapportera felet (se informationen nedan).</li>
          <li>Pröva att gå <a href="<?= get_option('home'); ?>/">tillbaks till startsidan</a></li>
          <li>Spela lite <a href="https://guide.ffuniverse.nu/">Final Fantasy</a> istället och försök igen lite senare.</li>
        </ul>

      </div>
    </div>
	</div>

        
	<div class="container message-sink">
    <div class="row">
      <div class="col-12">
        <p>
          <strong>Detaljerad information:</strong><br />
          <span class="weak">
            Sida/resurs du försökte nå: <strong>
            <?php
              // TODO: Nu med Wordpress så kan vi läsa URL direkt och skriva ut den eftersom denna inte skrivs om.
              $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
              $actual_link = str_replace("<","",$actual_link);
              $actual_link = str_replace(">","",$actual_link);
              echo $actual_link;

              if ($_GET['referer']) {
                //echo "Sida/resurs du försökte nå: <strong>";
                $referer = $_GET['referer'] + "";
                $referer = str_replace("<", "", $referer);
                $referer = str_replace(">", "", $referer);
                echo $referer;

                if ($_GET['qs']) {
                  $qs = $_GET['qs'] + "";
                  $qs = str_replace("<", "", $qs);
                  $qs = str_replace(">", "", $qs);
                  $qs = str_replace("!amp;", "&amp;", $qs);
                  $qs = str_replace("--", "=", $qs);
                  $qs = str_replace("%2F", "/", $qs);
                  echo "?" + $qs;
                }
                //echo "</strong><br />";
              }
				  ?>
            </strong><br />
            <?php /* Now() */ ?>
			    </span>
			  </p>
      </div>
    </div>
	</div>

	<div class="container">
    <div class="row">
      <div class="col-12">
        <p class="center">
          För att rapportera felet, så att vi kan laga det, ska du kontakta
          <a href="mailto:webmaster@ffuniverse.nu" style="color:#ea218e">Webmaster</a>.
          I det mailet ska du inkludera hela den detaljerade informationen ovan.
        </p>
      </div>
    </div>
	</div>
</main>

<?php get_footer(); ?>
