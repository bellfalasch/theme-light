<?php
/*
   Template Name: Updates
*/
global $pageTitle;

class GuideSite {
    var $updatesDbId;
    var $name;
    var $fullname;
    var $shortname;
    var $folder;
    var $guideserver;

    function setUpdatesDbId($value) { $this->updatesDbId = $value; }
    function getUpdatesDbId() { return $this->updatesDbId; }

    function setNameDefault($value) { $this->name = $value; }
    function getNameDefault() { return $this->name; }

    function setNameFull($value) { $this->fullname = $value; }
    function getNameFull() { return $this->fullname; }

    function setNameShort($value) { $this->shortname = $value; }
    function getNameShort() { return $this->shortname; }

    function setFolder($value) { $this->folder = $value; }
    function getFolder() { return $this->folder; }

    function setGuideServer($value) { $this->guideserver = $value; }
    function getGuideServer() { return $this->guideserver; }
}

$thisGuide = new GuideSite;
$paramGuide = get_query_var("guide", null);
if (!empty($paramGuide)) {
    setGuideData($paramGuide);
}

function setGuideData($guide) {
    global $thisGuide;

    // These are all the IDs we use in the updates database that we need to map to the correct guide.
    switch ($guide) {
    // Core series of Final Fantasy games:
    case 3:
    case "ff1":
        $thisGuide->setNameShort("FFI");
        $thisGuide->setNameFull("Final Fantasy I");
        $thisGuide->setNameDefault("Final Fantasy I");
        $thisGuide->setFolder("ff1");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(3);
        break;
    case 4:
    case "ff2":
        $thisGuide->setNameShort("FFII");
        $thisGuide->setNameFull("Final Fantasy II");
        $thisGuide->setNameDefault("Final Fantasy II");
        $thisGuide->setFolder("ff2");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(4);
        break;
    case 27:
    case "ff3":
        $thisGuide->setNameShort("FFIII");
        $thisGuide->setNameFull("Final Fantasy III");
        $thisGuide->setNameDefault("Final Fantasy III");
        $thisGuide->setFolder("ff3");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(27);
        break;
    case 5:
    case "ff4":
        $thisGuide->setNameShort("FFIV");
        $thisGuide->setNameFull("Final Fantasy IV");
        $thisGuide->setNameDefault("Final Fantasy IV");
        $thisGuide->setFolder("ff4");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(5);
        break;
    case 6:
    case "ff5":
        $thisGuide->setNameShort("FFV");
        $thisGuide->setNameFull("Final Fantasy V");
        $thisGuide->setNameDefault("Final Fantasy V");
        $thisGuide->setFolder("ff5");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(6);
        break;
    case 7:
    case "ff6":
        $thisGuide->setNameShort("FFVI");
        $thisGuide->setNameFull("Final Fantasy VI");
        $thisGuide->setNameDefault("Final Fantasy VI");
        $thisGuide->setFolder("ff6");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(7);
        break;
    case 8:
    case "ff7":
        $thisGuide->setNameShort("FFVII");
        $thisGuide->setNameFull("Final Fantasy VII");
        $thisGuide->setNameDefault("Final Fantasy VII");
        $thisGuide->setFolder("ff7");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(8);
        break;
    case 9:
    case "ff8":
        $thisGuide->setNameShort("FFVIII");
        $thisGuide->setNameFull("Final Fantasy VIII");
        $thisGuide->setNameDefault("Final Fantasy VIII");
        $thisGuide->setFolder("ff8");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(9);
        break;
    case 10:
    case "ff9":
        $thisGuide->setNameShort("FFIX");
        $thisGuide->setNameFull("Final Fantasy IX");
        $thisGuide->setNameDefault("Final Fantasy IX");
        $thisGuide->setFolder("ff9");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(10);
        break;
    case 11:
    case "ff10":
        $thisGuide->setNameShort("FFX");
        $thisGuide->setNameFull("Final Fantasy X");
        $thisGuide->setNameDefault("Final Fantasy X");
        $thisGuide->setFolder("ff10");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(11);
        break;
    case 12:
    case "ff11":
        $thisGuide->setNameShort("FFXI");
        $thisGuide->setNameFull("Final Fantasy XI");
        $thisGuide->setNameDefault("Final Fantasy XI");
        $thisGuide->setFolder("ff11");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(12);
        break;
    case 13:
    case "ff12":
        $thisGuide->setNameShort("FFXII");
        $thisGuide->setNameFull("Final Fantasy XII");
        $thisGuide->setNameDefault("Final Fantasy XII");
        $thisGuide->setFolder("ff12");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(13);
        break;
    case 25:
    case "ff13":
        $thisGuide->setNameShort("FFXIII");
        $thisGuide->setNameFull("Final Fantasy XIII");
        $thisGuide->setNameDefault("Final Fantasy XIII");
        $thisGuide->setFolder("ff13");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(25);
        break;
    case 28:
    case "ff15":
        $thisGuide->setNameShort("FFXV");
        $thisGuide->setNameFull("Final Fantasy XV");
        $thisGuide->setNameDefault("Final Fantasy XV");
        $thisGuide->setFolder("ff15");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(28);
        break;

    // Other Final Fantasy games (spinoffs):
    case 14:
    case "ff10-2":
        $thisGuide->setNameShort("FFX-2");
        $thisGuide->setNameFull("Final Fantasy X-2");
        $thisGuide->setNameDefault("Final Fantasy X-2");
        $thisGuide->setFolder("ff10-2");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(14);
        break;
    case 15:
    case "ff-tactics-advance":
        $thisGuide->setNameShort("FFTA");
        $thisGuide->setNameFull("Final Fantasy Tactics Advance");
        $thisGuide->setNameDefault("FF Tactics Advance");
        $thisGuide->setFolder("ff-tactics-advance");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(15);
        break;/*
    case 16:
    case "ff7ac":
        $thisGuide->setNameShort("FF7:AC");
        $thisGuide->setNameFull("Final Fantasy VII Advent Children");
        $thisGuide->setNameDefault("FFVII Advent Children");
        $thisGuide->setFolder("ff7ac");
        $thisGuide->setGuideServer(false);
        $thisGuide->setUpdatesDbId(16);
        break;*/
    case 22:
    case "ff-crystal-chronicles":
        $thisGuide->setNameShort("FFCC");
        $thisGuide->setNameFull("Final Fantasy Crystal Chronicles");
        $thisGuide->setNameDefault("FF Crystal Chronicles");
        $thisGuide->setFolder("ff-crystal-chronicles");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(22);
        break;
    case 29:
    case "ff7-remake":
        $thisGuide->setNameShort("FF7R");
        $thisGuide->setNameFull("Final Fantasy VII Remake");
        $thisGuide->setNameDefault("FFVII Remake");
        $thisGuide->setFolder("ff7-remake");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(29);
        break;

    // Other Square Enix games:
    case 17:
    case "kingdom-hearts":
        $thisGuide->setNameShort("KH");
        $thisGuide->setNameFull("Kingdom Hearts");
        $thisGuide->setNameDefault("Kingdom Hearts");
        $thisGuide->setFolder("kingdom-hearts");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(17);
        break;
    case 19:
    case "chrono-trigger":
        $thisGuide->setNameShort("CT");
        $thisGuide->setNameFull("Chrono Trigger");
        $thisGuide->setNameDefault("Chrono Trigger");
        $thisGuide->setFolder("chrono-trigger");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(19);
        break;
    case 23:
    case "brave-fencer-musashi":
        $thisGuide->setNameShort("BFM");
        $thisGuide->setNameFull("Brave Fencer Musashi");
        $thisGuide->setNameDefault("Brave Fencer Musashi");
        $thisGuide->setFolder("brave-fencer-musashi");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(23);
        break;
    case 26:
    case "bravely-default":
        $thisGuide->setNameShort("BD");
        $thisGuide->setNameFull("Bravely Default");
        $thisGuide->setNameDefault("Bravely Default");
        $thisGuide->setFolder("bravely-default");
        $thisGuide->setGuideServer(true);
        $thisGuide->setUpdatesDbId(26);
        break;

    default:
        $thisGuide->setUpdatesDbId(null);
        break;
    }
}
if ($thisGuide->getUpdatesDbId() !== null) {
    custom_title(["title" => "Uppdatering på " . $thisGuide->getNameDefault() . "-guiden"]);
} else {
    $pageTitle = "";
}
?><?php get_header(); ?>

<main class="container-fluid updates">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   			<h1>Final Fantasy Universe <small><?php the_title(); ?></small></h1>
   			<?php the_content(); ?>
          <?php endwhile; endif; ?>
        </div>
      </div>

      <?php
        //$database = "ffuniver_6";
        $table_name = $wpdb->prefix . 'ffu_updates';
        //$wpdb = new wpdb(DB_USER, DB_PASSWORD, $database, DB_HOST);
        //$wpdb->show_errors();

        if (!empty($paramGuide)) {
          //setGuideData($paramGuide);
          $searchString = " AND site = " . $thisGuide->getUpdatesDbId();
        } else {
          $searchString = "";
        }
        $start = 0;
        $walk = 10;

        $SQL_countAllUpdates = "
          SELECT COUNT(*)
          FROM " . $table_name . "
          WHERE ffu = 1
          " . $searchString . "
          AND site <> 16
          AND site <> 20
          AND site <> 21
          AND site > 2
        ;";

        $SQL_getAllUpdates = "
          SELECT published, site, headline, info, page, wp_fullurl
          FROM " . $table_name . "
          WHERE ffu = 1
          " . $searchString . "
          AND site <> 16
          AND site <> 20
          AND site <> 21
          AND site > 2
          ORDER BY published DESC
          LIMIT " . $start . ", " . $walk . "
        ;";

        // Handle counting of full amount of updates, regardless of pagination.
        $amountOfUpdates = 0;
        $counter = $wpdb->get_var( $SQL_countAllUpdates );
        if ($counter) { $amountOfUpdates = $counter; }

        $results = $wpdb->get_results( $SQL_getAllUpdates );

        // Did we find any data from before?
        if ($results) {

          echo '<div class="row">';
          echo '  <div class="col-1 hidden-sm">&nbsp;</div>';

          //echo $paramGuide;

         if (!empty($paramGuide)) {
            $guideUrl = "/" . $thisGuide->getFolder() . "/";
            //if (!$thisGuide->getGuideServer()) {
            //   $guideUrl = "http://www.ffuniverse.nu" . $guideUrl;
            //}
            echo '  <div class="col-4 col-12-sm">';
            echo "    <p class='centered'><a href='" . $guideUrl . "' class='game-logo' title='Klicka för att gå till " . $thisGuide->getNameDefault() . "-guiden'><img src='" . get_template_directory_uri() . "/assets/logos/" . $thisGuide->getFolder() . ".png' alt='Logo till " . $thisGuide->getNameDefault() . ".' /></a></p>";
            echo '  </div>';
            echo '  <div class="col-6 col-12-sm game-information">';
            echo "    <h2 class='game-name'>" . $thisGuide->getNameDefault() . " <small>" . $thisGuide->getNameShort() . "</small></h2>";
            echo "    <h6 class='category-badge'><a href='" . $guideUrl . "'>Öppna " . $thisGuide->getNameDefault() . "-guiden</a></h6>";
            echo '  </div>';
            echo '  <div class="col-1 hidden-sm">&nbsp;</div>';
            echo '</div><div class="row">';
            echo '  <div class="col-1 hidden-sm">&nbsp;</div>';
            echo '  <div class="col-10 col-12-sm">';
         } else {
            //echo '<div class="row">';
            //echo '  <div class="col-1">&nbsp;</div>';
            echo '  <div class="col-10 col-12-sm">';
            echo "    <h2>Alla guider <small>senaste uppdateringarna</small></h2>";
         }

          echo '<p><span class="badge-purple">' . $amountOfUpdates . '</span> uppdateringar totalt';
          if ($amountOfUpdates > $walk) { echo ' (bara ' . $walk . ' visas)'; }
          echo ': ';
          if (!empty($paramGuide)) {
          ?><a href="https://guide.ffuniverse.nu/<?= $thisGuide->getFolder(); ?>/feed/" class="blockRSSButton">Få detta som en RSS-feed istället</a><?php
          }
          echo '</p>';

          foreach($results as $update) {
            // If the URL param for selected guide is empty, we need to set game data for each iteration within the loop.
            if (empty($paramGuide)) {
               setGuideData($update->site);
               $guideUrl = "/" . $thisGuide->getFolder() . "/";
               //if (!$thisGuide->getGuideServer()) {
               //  $guideUrl = "http://www.ffuniverse.nu" . $guideUrl;
               //}
            } else {
                custom_title('Test');
            }
            //date("l d F \(Y\)\, \k\l. H:i", $update->published);
            $originalDate = strtotime($update->published);
            $day = strDateToSwedish("D", $originalDate);
            $month = strtolower(strDateToSwedish("M", $originalDate));
            $prettyDate = $day . " den " . date("j", $originalDate) . " " . $month . " <small>- " . date("Y\, \k\l. H:i", $originalDate) . "</small>";

            $body = $update->info;
            if (mb_substr($body,0,2) != '<p') {
              $body = "<p>" . $body . "</p>";
            }

            echo "<div class='update'>";
            if (empty($paramGuide)) {
              echo "<a href='" . $guideUrl . "' class='game-logo' title='Klicka för att gå till " . $thisGuide->getNameDefault() . "-guiden'><img src='" . get_template_directory_uri() . "/assets/logos/" . $thisGuide->getFolder() . ".png' alt='Logo till " . $thisGuide->getNameDefault() . ".' /></a>";
              echo "<h6 class='category-badge'><a href='" . $guideUrl . "' title='Klicka för att gå till " . $thisGuide->getNameDefault() . "-guiden'>" . $thisGuide->getNameDefault() . "</a></h6>";
            }
            echo "<h3><a href='" . $update->wp_fullurl . "'>" . $update->headline . "</a></h3>";
            echo "<p class='timestamp'><time datetime='" . str_replace(' ', 'T', $update->published) . "' title='" . $update->published . "'>" . $prettyDate . "</time></p>";
            echo "" . $body . "";
            echo "</div>";
          }
          echo '  </div>';
          echo '  <div class="col-1 hidden-sm">&nbsp;</div>';
          echo '</div>';
        }
      ?>
    </div><!-- .container -->

    <?php if (!empty($paramGuide)) { ?>
    <div class="container">
      <div class="row">
        <p>
          Det var alla uppdateringar vi hade om <strong><?= $thisGuide->getNameDefault() ?></strong>, titta gärna tillbaka senare.
          <a href="https://guide.ffuniverse.nu/<?= $thisGuide->getFolder(); ?>/feed/">Få detta som en RSS-feed istället</a>.
        </p>
      </div>
    </div>
    <?php } ?>

  </div>
</main>

<?php get_footer(); ?>
