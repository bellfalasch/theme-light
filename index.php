<?php get_header(); ?>

<main class="container-fluid game-logos">
  <div class="row">

<?php

  // NOTE: url for logo (name of file) must be consistent with name of folder/url for the site itself!
  // NOTE: server url must always be same, only part that can change is the subdomain!

  $utm_root = "?utm_source=GuideSelector&amp;utm_medium=[[[MEDIUM]]]&amp;utm_campaign=FFU2.5";
  $utm_guides = str_replace("[[[MEDIUM]]]", "GameList", $utm_root);
  $utm_footer_sites = str_replace("[[[MEDIUM]]]", "FooterSites", $utm_root);
  $utm_footer_about = str_replace("[[[MEDIUM]]]", "FooterAboutus", $utm_root);

  function generateLogoBox($url, $full, $server = 'guide', $linkable = true) {
    global $utm_guides; // Make this variable accessible from within the function too. Weird PHP ...
    $serverFull = "https://" . $server . ".ffuniverse.nu/";
	 if ($server === 'www') { $serverFull = str_replace('https://','http://',$serverFull); } // Support linking to https on guide-server.
    $html  = "<li class=\"col-4 col-12-sm flex-item game-logo\">";
    if ($linkable) {
      $html .= "<a href=\"" . $serverFull . $url . "/" . $utm_guides . "\" class=\"logo-anchor\">";
    } else {
      $html .= "<a href=\"\" class=\"no-anchor\">";
    }
    $html .= "<img src=\"" . get_template_directory_uri() . "/assets/logos/" . $url . ".png\" alt=\"Logon till " . $full . "\"";
    $html .= " title=\"";
    if ($linkable) {
      $html .= "Klicka f&ouml;r att l&auml;sa guiden till " . $full;
    } else {
      $html .= "Tyv&auml;rr har vi ingen guide till " . $full . ", &auml;n";
      $html .= "\" class=\"no-site";
    }
    $html .= "\" />";
    $html .= "<h3 class='category-badge'>" . $full . "</h3>";
    $html .= "</a>";
    echo $html . "</li>";
  }

  $onGuideServer = get_blog_count(); // Faktiskt antal sajter i nätverket (tar med icke-publicerade samt root-sajten).
  $secretSites = 0; // Öka denna när vi har sajter under utveckling som inte är publicerade än.
  $sumOfAllGuides = $onGuideServer - $secretSites - 1; // Tillsammans får vi totalt antal guider. Vi drar ifrån 1 för funktionen räknar root-sajten som en guide.

?>

    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1>Final Fantasy Universe <small>v&auml;lj ett spel</small></h1>
          <p>
            <strong>FFUniverse.nu</strong> har i <?= ( date("Y") - 1997 ); ?> år skrivit alla sina guider och sitt
            material själva, helt på svenska. Vi har massor av <strong>spelguider</strong> - <?= $sumOfAllGuides ?> stycken - för att hjälpa dig med
            <strong>Final Fantasy</strong>-spelen, och många andra spel från <strong>Square Enix</strong>.
            Har du kört fast, behöver hjälp med en extra svår boss, lösa en sista side-quest, eller vill du hitta det bästa
            vapnet eller magin? Då är FFUs guider för dig!
            Vi skriver också <a href="https://nyheter.ffuniverse.nu/">nyheter om Final Fantasy och Square Enix</a>.
          </p>
          <p>
            Klicka på en av spelens logos här nedan för att komma igång:
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <h2>Final Fantasy <small>grundserien</small></h2>
        </div>
      </div>
      <ol class="row flex-me logos">
        <?php generateLogoBox("ff1", "Final Fantasy I"); ?>
        <?php generateLogoBox("ff2", "Final Fantasy II"); ?>
        <?php generateLogoBox("ff3", "Final Fantasy III"); ?>
        <?php generateLogoBox("ff4", "Final Fantasy IV"); ?>
        <?php generateLogoBox("ff5", "Final Fantasy V"); ?>
        <?php generateLogoBox("ff6", "Final Fantasy VI"); ?>
        <?php generateLogoBox("ff7", "Final Fantasy VII"); ?>
        <?php generateLogoBox("ff8", "Final Fantasy VIII"); ?>
        <?php generateLogoBox("ff9", "Final Fantasy IX"); ?>
        <?php generateLogoBox("ff10","Final Fantasy X"); ?>
        <?php generateLogoBox("ff11","Final Fantasy XI"); ?>
        <?php generateLogoBox("ff12","Final Fantasy XII"); ?>
        <?php generateLogoBox("ff13","Final Fantasy XIII"); ?>
        <?php generateLogoBox("ff14","Final Fantasy XIV", "guide", false); ?>
        <?php generateLogoBox("ff15","Final Fantasy XV"); ?>
      </ol>

      <div class="row" id="fler">
        <div class="col-12">
          <h2>Final Fantasy <small>&ouml;vriga spel</small></h2>
        </div>
      </div>
      <ol class="row flex-me logos">
        <?php generateLogoBox("ff-crystal-chronicles", "Final Fantasy Crystal Chronicles"); ?>
        <?php generateLogoBox("ff-tactics-advance",    "Final Fantasy Tactics Advance"); ?>
        <?php generateLogoBox("ff10-2",                "Final Fantasy X-2"); ?>
        <?php generateLogoBox("ff7-remake",            "Final Fantasy VII Remake"); ?>
      </ol>

      <div class="row">
        <div class="col-12">
          <h2>Square Enix <small>&ouml;vriga spel</small></h2>
        </div>
      </div>
      <ol class="row flex-me logos">
        <?php generateLogoBox("chrono-trigger",   "Chrono Trigger"); ?>
        <?php generateLogoBox("kingdom-hearts",   "Kingdom Hearts"); ?>
        <?php generateLogoBox("bravely-default",  "Bravely Default"); ?>
        <?php generateLogoBox("brave-fencer-musashi","Brave Fencer Musashi"); ?>
        <?php generateLogoBox("kingdom-hearts-2", "Kingdom Hearts II",    "guide", false); ?>
      </ol>

    </div>

    <div class="container">
      <div class="row">
        <div class="col-12">
          <p class="centered">
            Detta var alla spelguider vi har att erbjuda just nu - men vi är alltid <a href="https://om.ffuniverse.nu/hjalp-oss/bli-en-del-av-ffu/">intresserade av fler</a>, men <strong>vi behöver din hjälp</strong>!
          </p>
          <p class="centered">
            
            Det finns också en massa annat på FFU. Ta en titt på länkarna som följer här nedanför.
          </p>
        </div>
      </div>
    </div>
</main>

<?php get_footer(); ?>
