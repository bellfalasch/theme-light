<?php

add_filter('document_title_parts', 'custom_title');
function custom_title( $title ) {
  // $title is an array of title parts, including one called `title`

  //$title['title'] = 'My new title';

	if ( !(is_404()) && (is_single()) || (is_page()) ) {
		// This is a normal page with a parent:
		if ( is_page() ) {
			if ($pageTitle !== null) {
				$title['title'] = $pageTitle;
			}
		}
	} elseif (is_404()) {
		$title['title'] = 'Sidan kunde inte hittas';
	}
	if ( is_home() ) {
		$title['title'] = bloginfo('description');
	}
	return $title;
}

add_action('init','add_get_val');
function add_get_val() {
	global $wp;
	$wp->add_query_var('guide');
}

function strDateToSwedish($what, $thedate) {
	// From day number set day name
	$daynum = date("N", $thedate );
	switch($daynum) {
		case 1:	 $day = "Måndag";  break;
		case 2:	 $day = "Tisdag";  break;
		case 3:	 $day = "Onsdag";  break;
		case 4:	 $day = "Torsdag"; break;
		case 5:	 $day = "Fredag";  break;
		case 6:	 $day = "Lördag";  break;
		case 7:	 $day = "Söndag";  break;
		default: $day = "Okänt";   break;
	}

	// From month number set month name
	$monthnum  = date("n", $thedate );
	switch($monthnum) {
		case 1:  $month = "Januari";   break;
		case 2:  $month = "Februari";  break;
		case 3:  $month = "Mars";      break;
		case 4:  $month = "April";     break;
		case 5:  $month = "Maj";       break;
		case 6:  $month = "Juni";      break;
		case 7:  $month = "Juli";      break;
		case 8:  $month = "Augusti";   break;
		case 9:  $month = "September"; break;
		case 10: $month = "Oktober";   break;
		case 11: $month = "November";  break;
		case 12: $month = "December";  break;
		default: $month = "Okänt";     break;
	}

	// Depending on what we wanted to get from the function return that
	$returnthis = '';
	switch( strtoupper($what) )
	{
		case 'M': $returnthis = $month;	break;
		case 'D': $returnthis = $day;	break;
	}
	return htmlentities($returnthis, ENT_QUOTES, "UTF-8");
}

?>
