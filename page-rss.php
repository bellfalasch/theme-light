<?php
/*
   Template Name: RSS för updates
*/
global $wpdb;

$postCount = 20;
$table_name = $wpdb->prefix . 'ffu_updates';
//$posts = query_posts('showposts=' . $postCount);
$updates = $wpdb->get_results( "SELECT * FROM `{$table_name}` WHERE `rss` = 1 AND `site` <> 20 AND `site` <> 21 AND `site` <> 18 AND `site` <> 16 ORDER BY `published` DESC LIMIT 20", OBJECT );

header('Content-Type: application/rss+xml');
header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);

echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<rss version="2.0"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:wfw="http://wellformedweb.org/CommentAPI/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
    <?php do_action('rss2_ns'); ?>>

    <channel>
        <title>Final Fantasy Universe - Uppdateringar</title>
	      <category>News</category>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <link>https://guide.ffuniverse.nu/sista-andringarna/</link>
        <description>Senaste uppdateringarna på våra Square Enix och Final Fantasy-guider, och andra - FFU www.ffuniverse.nu</description>
        <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0200', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
        <language>sv</language>
        <copyright>Copyright 1997-<?= date("Y"); ?> ffuniverse.nu</copyright>
        <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', 'daily' ); ?></sy:updatePeriod>
        <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', '1' ); ?></sy:updateFrequency>
	      <ttl>15</ttl>
        <?php do_action('rss2_head'); ?>
        <?php foreach ( $updates as $update ) { ?>
            <item>
            <?php
                switch ($update->site) {
                    case 0 :
                        $guideName = "Allmänt";
                        $guideFolder = "";
                        break;
                    case 1 :
                        $guideName = "Nyhetssajten";
                        $guideFolder = "";
                        break;
                    case 2 :
                        $guideName = "Communityn";
                        $guideFolder = "community/";
                        break;
                    case 3 :
                        $guideName = "FFI";
                        $guideFolder = "ff1/";
                        break;
                    case 4 :
                        $guideName = "FFII";
                        $guideFolder = "ff2/";
                        break;
                    case 27:
                        $guideName = "FFIII";
                        $guideFolder = "ff3/";
                        break;
                    case 5 :
                        $guideName = "FFIV";
                        $guideFolder = "ff4/";
                        break;
                    case 6 :
                        $guideName = "FFV";
                        $guideFolder = "ff5/";
                        break;
                    case 7 :
                        $guideName = "FFVI";
                        $guideFolder = "ff6/";
                        break;
                    case 8 :
                        $guideName = "FFVII";
                        $guideFolder = "ff7/";
                        break;
                    case 9 :
                        $guideName = "FFVIII";
                        $guideFolder = "ff8/";
                        break;
                    case 10:
                        $guideName = "FFIX";
                        $guideFolder = "ff9/";
                        break;
                    case 11:
                        $guideName = "FFX";
                        $guideFolder = "ff10/";
                        break;
                    case 12:
                        $guideName = "FFXI";
                        $guideFolder = "ff11/";
                        break;
                    case 13:
                        $guideName = "FFXII";
                        $guideFolder = "ff12/";
                        break;
                    case 25:
                        $guideName = "FFXIII";
                        $guideFolder = "ff13/";
                        break;
                    case 28:
                        $guideName = "FFXV";
                        $guideFolder = "ff15/";
                        break;
                    case 14:
                        $guideName = "FFX-2";
                        $guideFolder = "ff10-2/";
                        break;
                    case 15:
                        $guideName = "FFTA";
                        $guideFolder = "ff-tactics-advance/";
                        break;
                    case 22:
                        $guideName = "FFCC";
                        $guideFolder = "ff-crystal-chronicles/";
                        break;
                    case 29:
                        $guideName = "FF7R";
                        $guideFolder = "ff7-remake/";
                        break;
                    case 17:
                        $guideName = "Kingdom Hearts";
                        $guideFolder = "kingdom-hearts/";
                        break;
                    case 19:
                        $guideName = "Chrono Trigger";
                        $guideFolder = "chrono-trigger/";
                        break;
                    case 23:
                        $guideName = "Brave Fencer Musashi";
                        $guideFolder = "bfm/";
                        break;
                    case 26:
                        $guideName = "Bravely Default";
                        $guideFolder = "bravely-default/";
                        break;
                }
                $body_with_category = $update->info;
                if ( substr($body_with_category,0,3) === "<p>") {
                  $body_with_category = "<p>" . $guideName . ": " . substr($body_with_category,3);
                } else {
                  $body_with_category = "<p>" . $guideName . ": " . $body_with_category . "</p>";
                }
            ?>
                <title><![CDATA[<?= $update->headline; ?>]]></title>
                <link><?= $update->wp_fullurl; ?></link>
                <pubDate><?php echo mysql2date('D, d M Y H:i:s +0200', $update->published, false); ?></pubDate>
                <guid isPermaLink="false"><?= $update->wp_fullurl; ?></guid>
                <description><![CDATA[Guiden till <?= $guideName ?> på FFUniverse är uppdaterad! Ändringen har titeln "<?= $update->headline; ?>".]]></description>
                <content:encoded><![CDATA[<?= $body_with_category ?>]]></content:encoded>
                <category><?= $guideName ?></category>
                <?php rss_enclosure(); ?>
                <?php do_action('rss2_item'); ?>
            </item>
        <?php } ?>
    </channel>
</rss>